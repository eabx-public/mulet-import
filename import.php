<?php

/**
 * Created by Eric Quinton - April 2023
 * Version 1.0 
 * Copyright © INRAE
 * MIT License
 */

class ImportException extends Exception
{
};

error_reporting(E_ERROR);
require_once "lib/ObjetBDD_functions.php";
require_once "lib/ObjetBDD.php";
require_once "lib/functions.php";
require_once "lib/message.class.php";
require_once "lib/csv.class.php";
$message = new Message();
objetBDDparamInit();
/**
 * End of Treatment
 */
$eot = false;

/**
 * Default options
 */
$message->set("Import of data for database mulet");
$message->set("Licence : MIT. Copyright © 2023 - Éric Quinton, INRAE - EABX - FR33 Cestas");
/**
 * Traitement des options de la ligne de commande
 */
if ($argv[1] == "-h" || $argv[1] == "--help") {

    $message->set("Options:");
    $message->set("-h or --help: this help message");
    $message->set("All parameters in the section [general] of the file param.ini can be used");
    $eot = true;
} else {
    /**
     * Processing args
     */
    $moveFile = true;
    $numBac = "";
    $params = array();
    for ($i = 1; $i <= count($argv); $i++) {
        $arg = explode("=", $argv[$i]);
        $params[$arg[0]] = $arg[1];
    }
}
$message->display();
if (!$eot) {
    if (!isset($params["param"])) {
        $params["param"] = "./param.ini";
    }
    /**
     * Get the parameters from param.ini file
     *
     */
    if (!file_exists($params["param"])) {
        $message->set("The parameter file " . $params["param"] . " don't exists");
        $eot = true;
    } else {
        $param = parse_ini_file($params["param"], true);
        foreach ($params as $key => $value) {
            $param["general"][substr($key, 2)] = $value;
        }
    }
    /**
     * Execution
     */
    objetBDDparamInit();
    /**
     * Extract the secret key
     */

    $secretKey = file_get_contents($param["general"]["secretKey"]);
    try {
        /**
         * Connexion to the database
         */
        $dbmodel = $param["general"]["dbmodel"];
        $pdo = new PDO(
            $param[$dbmodel]["dsn"],
            $param[$dbmodel]["login"],
            openssl_decrypt($param[$dbmodel]["password"],"aes-256-cbc", $secretKey)
        );
        $pdo->beginTransaction();
        $pdo->exec("set search_path = ".$param[$dbmodel]["searchPath"]);
        if (array_key_exists("action", $param["general"]) && !empty($param["general"])) {
            $action = $param["general"]["action"];
            if (array_key_exists($action, $param)) {
                $module = $param[$action];
                if (file_exists($module["program"])) {
                    include $module["program"];
                } else {
                    throw new ImportException("The program to execute does not exists");
                }
            } else throw new ImportException("The section of the action $action is not defined into the param.ini file");
        }
        $pdo->commit();
    } catch (ImportException $ie) {
        $message->set($ie->getMessage());
        if (isset($pdo)) {
            $pdo->rollBack();
        }
    }
}

$message->display();

