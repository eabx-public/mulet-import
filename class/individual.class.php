<?php
class Individual extends ObjetBDD
{
    function __construct(PDO $bdd, $param = array())
    {
        $this->table = "individual";
        $this->colonnes = array(
            "individual_id" => array("type" => 1, "requis" => 1, "key" => 1, "defaultValue" => 0),
            "sample_id" => array("type" => 1, "requis" => 1),
            "stage_id" => array("type" => 1),
            "total_length" => array("type" => 1),
            "standard_length" => array("type" => 1),
            "fork_length" => array("type" => 1),
            "weight" => array("type" => 1),
            "tag" => array("type" => 0),
            "identifier"=>array("type"=>0),
            "age"=>array("type"=>1)
        );
        parent::__construct($bdd, $param);
    }

    function getListIdsFromOriginKeys(int $origin_id): array
    {
        $sql = "select i.individual_id, i.origin_key 
                from individual i
                join sample using (sample_id)
                where origin_id = :origin";
        $data = $this->getListeParamAsPrepared($sql, array("origin" => $origin_id));
        $list = array();
        foreach ($data as $row) {
            $list[$row["origin_key"]] = $row["individual_id"];
        }
        return $list;
    }

    function getListIdsFromIdentifier(int $origin_id): array
    {
        $sql = "select i.individual_id, i.identifier 
                from individual i
                join sample using (sample_id)
                where origin_id = :origin";
        $data = $this->getListeParamAsPrepared($sql, array("origin" => $origin_id));
        $list = array();
        foreach ($data as $row) {
            $list[$row["identifier"]] = $row["individual_id"];
        }
        return $list;
    }
    function getIdFromIdentifier(int $origin_id, string $identifier): int
    {
        $sql = "select i.individual_id, i.identifier 
                from individual i
                join sample using (sample_id)
                where origin_id = :origin
                and identifier = :identifier";
        $data = $this->lireParamAsPrepared($sql, array("origin" => $origin_id, "identifier" => $identifier));
        if ($data["individual_id"] > 0) {
            return $data["individual_id"];
        } else {
            return 0;
        }
    }
}
