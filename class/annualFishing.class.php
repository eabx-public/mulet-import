<?php
class AnnualFishing extends ObjetBDD
{
    function __construct(PDO $bdd, $param = array())
    {
        $this->table = "annual_fishing";
        $this->colonnes = array(
            "annual_fishing_id" => array("type" => 1, "requis" => 1, "key" => 1, "defaultValue" => 0),
            "taxon_id" => array("type"=>1, "requis"=>1),
            "origin_id"=>array("type"=>1, "requis"=>1),
            "fishing_zone" => array("type" => 0),
            "year" => array("type" => 1),
            "live_weight"=>array("type"=>1),
            "fao_area"=>array("type"=>1)
        );
        parent::__construct($bdd, $param);
    }
}
