<?php
class WeekFishing extends ObjetBDD
{
    function __construct(PDO $bdd, $param = array())
    {
        $this->table = "week_fishing";
        $this->colonnes = array(
            "week_fishing_id" => array("type" => 1, "requis" => 1, "key" => 1, "defaultValue" => 0),
            "taxon_id" => array("type"=>1, "requis"=>1),
            "origin_id"=>array("type"=>1, "requis"=>1),
            "harbour" => array("type" => 0, "requis"=>1),
            "year" => array("type" => 1),
            "week"=>array("type"=>1),
            "live_weight"=>array("type"=>1),
            "amount"=>array("type"=>1)
        );
        parent::__construct($bdd, $param);
    }
}
