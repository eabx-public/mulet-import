<?php
class Engine extends ObjetBDD
{
    private array $engines = array();
    function __construct(PDO $bdd, $param = array())
    {
        $this->table = "engine";
        $this->colonnes = array(
            "engine_id" => array("type" => 1, "requis" => 1, "key" => 1, "defaultValue" => 0),
            "engine_name" => array("type" => 0, "requis" => 0),
            "engine_description" => array("type" => 0)
        );
        parent::__construct($bdd, $param);
    }

    function getIdFromName(string $name, string $code = "", bool $create = true)
    {
        if (key_exists($name, $this->engines)) {
            return $this->engines[$name];
        } else {
            $sql = "select engine_id, engine_name from engine
                    where engine_name = :name";
                    $param = array("name" => $name);
                    if (!empty($code)) {
                        $sql .= " and engine_code = :code";
                        $param["code"] = $code;
                    }
            $data = $this->lireParamAsPrepared($sql, $param);
            if (!$data["engine_id"] > 0 && $create) {
                $data = array(
                    "engine_id" => 0,
                    "engine_name" => $name,
                    "engine_code" => $code
                );
                $data["engine_id"] = $this->ecrire($data);
            }
            $this->engines[$name] = $data["engine_id"];
            return ($data["engine_id"]);
        }
    }
}
