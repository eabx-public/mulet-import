<?php
class Origin extends ObjetBDD
{
    private array $origins = array();
    function __construct(PDO $bdd, $param = array())
    {
        $this->table = "origin";
        $this->colonnes = array(
            "origin_id" => array("type" => 1, "requis" => 1, "key" => 1, "defaultValue" => 0),
            "origin_name" => array("type" => 0, "requis" => 0),
            "origin_description" => array("type" => 0),
            "origin_rights"=>array("type"=>0)
        );
        parent::__construct($bdd, $param);
    }

    function getIdFromName(string $name, bool $create = true)
    {
        if (key_exists($name, $this->origins)) {
            return $this->origins[$name];
        } else {
            $sql = "select origin_id, origin_name from origin
                    where origin_name = :name";
            $data = $this->lireParamAsPrepared($sql, array("name" => $name));
            if (!$data["origin_id"] > 0 && $create) {
                $data = array(
                    "origin_id" => 0,
                    "origin_name" => $name
                );
                $data["origin_id"] = $this->ecrire($data);
            }
            $this->origins[$name] = $data["origin_id"];
            return ($data["origin_id"]);
        }
    }
}
