<?php
class Sample extends ObjetBDD
{
    function __construct(PDO $bdd, $param = array())
    {
        $this->table = "sample";
        $this->colonnes = array(
            "sample_id" => array("type" => 1, "requis" => 1, "key" => 1, "defaultValue" => 0),
            "origin_id" => array("type" => 1, "requis" => 1),
            "taxon_id" => array("type" => 1, "requis" => 1),
            "engine_id" => array("type" => 1),
            "stage_id" => array("type" => 1),
            "sample_date" => array("type" => 2),
            "duration" => array("type" => 1),
            "depth" => array("type" => 1),
            "total_number" => array("type" => 1),
            "total_weight" => array("type" => 1),
            "trait_length" => array("type" => 1),
            "lon" => array("type" => 1),
            "lat" => array("type" => 1),
            "point_geom" => array("type" => 4),
            "coordinate_uncertainty" => array("type" => 1),
            "occurrence_status" => array("type" => 1),
            "ices_area" => array("type" => 0),
            "coordinate_precision" => array("type" => 0),
            "origin_precision" => array("type" => 0),
            "origin_key" => array("type" => 1)
        );
        $this->srid = 4326;
        parent::__construct($bdd, $param);
    }
    function getListIdsFromOriginKeys(int $origin_id): array
    {
        $sql = "select sample_id, origin_key from sample where origin_id = :origin";
        $data = $this->getListeParamAsPrepared($sql, array("origin" => $origin_id));
        $list = array();
        foreach ($data as $row) {
            $list[$row["origin_key"]] = $row["sample_id"];
        }
        return $list;
    }

    function getIdFromDateSite(int $origin_id, string $date, string $site)
    {
        $sql = "select sample_id from sample
                where origin_id = :origin
                and sample_date = :date
                and coordinate_precision = :site";
        $data = $this->lireParamAsPrepared($sql, array("origin" => $origin_id, "date" => $date, "site" => $site));
        if ($data["sample_id"] > 0) {
            return $data["sample_id"];
        } else {
            return 0;
        }
    }
}
