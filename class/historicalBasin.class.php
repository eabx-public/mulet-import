<?php
class HistoricalBasin extends ObjetBDD
{
    private array $engines = array();
    function __construct(PDO $bdd, $param = array())
    {
        $this->table = "historical_basin";
        $this->colonnes = array(
            "historical_basin_id" => array("type" => 1, "requis" => 1, "key" => 1, "defaultValue" => 0),
            "taxon_id" => array("type" => 1, "requis" => 0),
            "origin_id" => array("type" => 1, "requis"=>1),
            "presence_absence"=>array("type"=>1),
            "year_from"=>array("type"=>1),
            "year_to"=>array("type"=>1),
            "abundance_status"=>array("type"=>0),
            "abundance_comment"=>array("type"=>0),
            "seaoutlet_lon"=>array("type"=>1),
            "seaoutlet_lat"=>array("type"=>1),
            "seaoutlet_geom"=>array("type"=>4)
        );
        parent::__construct($bdd, $param);
    }
}