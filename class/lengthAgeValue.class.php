<?php
class LengthAgeValue extends ObjetBDD
{
    function __construct(PDO $bdd, $param = array())
    {
        $this->table = "length_age_value";
        $this->colonnes = array(
            "length_age_value_id" => array("type" => 1, "requis" => 1, "key" => 1, "defaultValue" => 0),
            "length_age_id" => array("type" => 1, "requis" => 1),
            "age" => array("type" => 1),
            "length" => array("type" => 1)
        );
        parent::__construct($bdd, $param);
    }
}
