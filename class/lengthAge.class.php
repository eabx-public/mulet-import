<?php
class LengthAge extends ObjetBDD {
    function __construct(PDO $bdd, $param = array())
    {
        $this->table = "length_age";
        $this->colonnes = array(
            "length_age_id" => array("type" => 1, "requis" => 1, "key" => 1, "defaultValue" => 0),
            "taxon_id" => array("type" => 1, "requis"=>1),
            "year"=>array("type"=>1),
            "author"=>array("type"=>0),
            "area"=>array("type"=>0),
            "geographic_area"=>array("type"=>0),
            "environment"=>array("type"=>0),
            "method"=>array("type"=>0),
            "lon"=>array("type"=>1),
            "lat"=>array("type"=>1),
            "geom"=>array("type"=>4)
        );
        $this->srid = 4326;
        parent::__construct($bdd, $param);
    }
}