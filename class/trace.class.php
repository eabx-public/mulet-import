<?php
class Trace extends ObjetBDD
{
    function __construct(PDO $bdd, $param = array())
    {
        $this->table = "trace";
        $this->colonnes = array(
            "trace_id" => array("type" => 1, "requis" => 1, "key" => 1, "defaultValue" => 0),
            "sample_id" => array("type" => 1, "requis" => 1),
            "start_lon" => array("type" => 1),
            "start_lat" => array("type" => 1),
            "end_lon" => array("type" => 1),
            "end_lat" => array("type" => 1),
            "trace_geom" => array("type" => 4)
        );
        $this->srid = 4326;
        parent::__construct($bdd, $param);
    }
}
