<?php
class Taxon extends ObjetBDD
{
    /**
     * List of precendent searched taxa
     *
     * @var array
     */
    private array $taxons = array();
    function __construct(PDO $bdd, $param = array())
    {
        $this->table = "taxon";
        $this->colonnes = array(
            "taxon_id" => array("type" => 1, "requis" => 1, "key" => 1, "defaultValue" => 0),
            "taxon_name" => array("requis" => 1),
            "vernacular_name" => array("type" => 0),
            "fao_code" => array("type" => 0)
        );
        parent::__construct($bdd, $param);
    }

    function getIdFromName(string $name, bool $create = true)
    {
        if (array_key_exists($name, $this->taxons)) {
            return $this->taxons[$name];
        } else {
            $data = $this->lireParamAsPrepared(
                "select taxon_id from taxon where taxon_name = :name",
                array("name" => $name)
            );
            if ($data["taxon_id"] == 0) {
                $data = array(
                    "taxon_id" => 0,
                    "taxon_name" => $name
                );
                $data["taxon_id"] = $this->ecrire($data);
            }
            $this->taxons[$name] = $data["taxon_id"];
            return $data["taxon_id"];
        }
    }
}
