<?php
class Physicochemical extends ObjetBDD
{
    function __construct(PDO $bdd, $param = array())
    {
        $this->table = "physicochemical";
        $this->colonnes = array(
            "physicochemical_id" => array("type" => 1, "requis" => 1, "key" => 1, "defaultValue" => 0),
            "sample_id" => array("type" => 1, "requis" => 1),
            "conductivity" => array("type" => 1),
            "salinity" => array("type" => 1),
            "oxygen_sat" => array("type" => 1),
            "oxygen_mgl" => array("type" => 1),
            "temperature" => array("type" => 1),
            "turbidity" => array("type" => 1),
            "salinity_class" => array("type" => 1),
            "ph"=>array("type"=>1)
        );
        parent::__construct($bdd, $param);
    }
}
