# README.md

L'application est conçue pour faciliter les importations de données CSV ou extraites d'une base de données dans une autre base de données. Elle a été créée pour alimenter la base de données "*mulet*" de l'unité de recherche EABX d'INRAE.

Le fichier `param-dist.ini` doit être renommé en `param.ini`. Il contient tous les paramètres nécessaires à l'exécution des modules, c'est à dire des scripts qui sont appelés de façon indépendante pour intégrer les données.

Le programme s'exécute en ligne de commande :

```bash
php import.php dbmodel=prod action=sturat
```

Pour, ici, exécuter le code décrit dans le fichier `modules/sturat.php` (entrée` [sturat]` du fichier `param.ini`), vers l'environnement de production.

Les mots de passe d'accès aux bases de données sont chiffrés dans le fichier `param.ini`.  Voici un exemple de génération du mot de passe chiffré :

1. Générez la clé secrète :

```bash
echo -n `pwgen 20 -N 1` > ~/.ssh/secret.key
```

2. Chiffrez le mot de passe à partir de la clé secrète, avec ce script :

```php
<?php
$secretKey = file_get_contents("~/.ssh/secret.key");
$password = "mot_de_passe_en_clair";
echo openssl_encrypt($password, "aes-256-cbc", $secretKey);
```

Puis exécutez le script :

```bash
php keyencrypt.php
```

3. Recopiez la clé chiffrée dans le fichier `param.ini`.

## Licence

Le code est disponible sous licence BSD.

© Éric Quinton pour EABX-INRAE - Août 2023.
