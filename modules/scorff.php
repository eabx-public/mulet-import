<?php
include_once "class/sample.class.php";
include_once "class/origin.class.php";
include_once "class/individual.class.php";
$individual = new Individual($pdo, $ObjetBDDParam);
$sample = new Sample($pdo, $ObjetBDDParam);
$sample->auto_date = 0;
$origin = new Origin($pdo, $ObjetBDDParam);
$origin_id = $origin->getIdFromName($module["origin"]);
$csv = new Csv();
$csv->initFile($module["filename"], $module["separator"]);
$eof = false;
$totalLines = 0;
$samplesRecorded = 0;
$individualRecorded = 0;
while (!$eof) {
    $line = $csv->getLineAsArray();
    if (!$line) {
        $eof = true;
    } else {
        $totalLines++;
        /**
         * Search for sample_id
         */
        $date = $sample->formatDateLocaleVersDB(substr($line["cam_date_heure_fin"],0,10));
        $riviere= "Scorff";
        if ($line["sec_code"] == "ETEL") {
            $riviere = "Etel";
        }
        $sample_id = $sample->getIdFromDateSite($origin_id, $date, $riviere);
        if ($sample_id == 0) {
            $point_geom = "POINT(-3.393926 47.83692)";
            $lon = -3.393926;
            $lat = 47.83692;
            if ($riviere != "Scorff") {
                $point_geom = "";
                $lon = "";
                $lat = "";
            }
            $dataSample = array(
                "sample_id" => 0,
                "origin_id" => $origin_id,
                "taxon_id" => 1,
                "sample_date" => $date,
                "coordinate_precision" => $riviere,
                "lon" => $lon,
                "lat" => $lat,
                "point_geom" => $point_geom
            );
            $sample_id = $sample->ecrire($dataSample);
            $samplesRecorded++;
        }
        $individual_id = $individual->getIdFromIdentifier($origin_id, $line["cap_num_capture"]);
        if ($individual_id == 0) {
            $dataIndividual = array(
                "individual_id" => 0,
                "sample_id" => $sample_id,
                "tag" => $line["mqg_numero"],
                "identifier" => $line["cap_num_capture"],
                "age" => str_replace("+","",filter_var($line["age_code"],FILTER_SANITIZE_NUMBER_INT)),
                "fork_length" => $line["cap_lf"],
                "weight" => $line["cap_poids"]
            );
            $individual->ecrire($dataIndividual);
            $individualRecorded++;
        }
    }
}
$message->set("$samplesRecorded recorded samples on a total of $totalLines treated");
$message->set("$individualRecorded individuals recorded");
$csv->fileClose();
