<?php
include_once "class/sample.class.php";
include_once "class/engine.class.php";
include_once "class/physicochemical.class.php";
include_once "class/origin.class.php";
include_once "class/trace.class.php";
include_once "lib/db.class.php";
$db = new Db();
$pdoPomet = $db->connect(
    $module["dsn"],
    $module["login"],
    openssl_decrypt($module["password"], "aes-256-cbc", $secretKey),
    $module["searchPath"]
);
$origin = new Origin($pdo, $ObjetBDDParam);
$origin_id = $origin->getIdFromName($module["origin"]);
$sample = new Sample($pdo, $ObjetBDDParam);
$sample->auto_date = 0;
$exists = $sample->getListIdsFromOriginKeys($origin_id);
$engine = new Engine($pdo, $ObjetBDDParam);
$physicochemical = new Physicochemical($pdo, $ObjetBDDParam);
$trace = new Trace($pdo, $ObjetBDDParam);

$totalNumber = 0;
$treated = 0;
$commit = 0;
$maxCommit = 1000;
/**
 * search the data into db pomet
 */
$sql = "select trait_id, madate::date as trait_start, ordre, ech_id as echantillon_id,
        e.experimentation_id, experimentation_libelle,
        campagne_nom,
        duree as duration,
        conductivite, maree, oxygene, profondeur, salinite, temperature, salinite_classe, ph,
        t.pos_deb_lat_dd as start_lat, t.pos_deb_long_dd as start_lon, t.pos_fin_lat_dd as end_lat, t.pos_fin_long_dd as end_lon,
        materiel_code,materiel_nom, 
        nt as total_number, pt as weight,
        case when dpg.point_geom is not null then st_astext(dpg.point_geom) else st_astext(st_centroid(trait_geom)) end as point_geom,
        st_astext (ligne_geom) as trace_geom
        from trait t 
        join echantillon on (trait_id = fk_trait_id)
        join materiel on (fk_materiel_id = materiel_id)
        join campagnes c on (fk_campagne_id = campagne_id)
        left outer join experimentation e on (c.experimentation_id = e.experimentation_id)
        left outer join tracegps t2 using (trait_id)
        left outer join dce_ligne_geom dlg using (trait_id)
        left outer join dce_point_geom dpg using (trait_id)
        where espece_id = 104
        order by  trait_start, ordre
        ";
$dataPomet = $db->execute($sql);
foreach ($dataPomet as $row) {
    $totalNumber++;
    if (!key_exists($row["echantillon_id"], $exists)) {
        $data = array(
            "sample_id" => 0,
            "origin_id" => $origin_id,
            "taxon_id" => 1,
            "engine_id" => $engine->getIdFromName($row["materiel_nom"],$row["materiel_code"]),
            "sample_date" => $row["trait_start"],
            "duration" => intval($row["duration"]),
            "total_number" => intval($row["total_number"]),
            "total_weight" => $row["weight"],
            "lon" => $row["st_x"],
            "lat" => $row["st_y"],
            "point_geom" => $row["point_geom"],
            "origin_precision" => $row["campagne_nom"],
            "origin_key" => $row["echantillon_id"]
        );
        $sample_id = $sample->ecrire($data);
        $physicochemical->ecrire(
            array(
                "physicochemical_id" => 0,
                "sample_id" => $sample_id,
                "conductivity" => $row["conductivite"],
                "oxigen_ppt" => $row["oxygene"],
                "temperature" => $row["temperature"],
                "turbidity" => $row["turbidity"],
                "salinity" => $row["salinite"],
                "salinity_class" => $row["salinite_classe"],
                "ph" => $row["ph"]
            )
        );
        if (!empty($row["trace_geom"])) {
            $trace->ecrire(
                array(
                    "trace_id" => 0,
                    "sample_id" => $sample_id,
                    "start_lon" => $row["start_lon"],
                    "start_lat" => $row["start_lat"],
                    "end_lon" => $row["end_lon"],
                    "end_lat" => $row["end_lat"],
                    "trace_geom" => $row["trace_geom"]
                )
            );
        }
        $treated++;
        $commit++;
        if ($commit >= $maxCommit) {
            $pdo->commit();
            $message->set("$treated items recorded");
            $message->display();
            $commit = 0;
            $pdo->beginTransaction();
        }
    }
}
$message->set("$treated items recorded on a total of $totalNumber lines");
