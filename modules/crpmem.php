<?php
include_once "class/origin.class.php";
include_once "class/annualFishing.class.php";
$annualFishing = new AnnualFishing($pdo, $ObjetBDDParam);
$origin = new Origin($pdo, $ObjetBDDParam);
$origin_id = $origin->getIdFromName($module["origin"]);
$csv = new Csv();
$csv->initFile($module["filename"], $module["separator"]);
$eof = false;
$totalLines = 0;
$recordedLines = 0;
$cols = array("Eaux continentales", "Eaux maritimes");
while (!$eof) {
    $line = $csv->getLineAsArray();
    $totalLines++;
    if (!$line) {
        $eof = true;
    } else {
        foreach ($cols as $col) {
            $annualFishing->ecrire(
                array(
                    "annual_fishing_id" => 0,
                    "taxon_id" => 4,
                    "origin_id" => $origin_id,
                    "fishing_zone" => $col,
                    "year" => $line["Annee"],
                    "live_weight" => $line[$col]
                )
            );
        }
        $recordedLines++;
    }
}
$message->set("$recordedLines recorded on a total of $totalLines treated");
$csv->fileClose();
