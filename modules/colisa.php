<?php
include_once "class/sample.class.php";
include_once "class/origin.class.php";
include_once "class/individual.class.php";
$individual = new Individual($pdo, $ObjetBDDParam);
$sample = new Sample($pdo, $ObjetBDDParam);
$sample->auto_date = 0;
$origin = new Origin($pdo, $ObjetBDDParam);
$origin_id = $origin->getIdFromName($module["origin"]);
$csv = new Csv();
$csv->initFile($module["filename"], $module["separator"]);
$eof = false;
$totalLines = 0;
$samplesRecorded = 0;
$individualRecorded = 0;
while (!$eof) {
    $line = $csv->getLineAsArray();
    if (!$line) {
        $eof = true;
    } else {
        $totalLines++;
        /**
         * Search for sample_id
         */
        $sample_id = $sample->getIdFromDateSite($origin_id, $line["date_capture"], $line["riviere"]);
        if ($sample_id == 0) {
            $point_geom = "POINT(-3.393926 47.83692)";
            $lon = -3.393926;
            $lat = 47.83692;
            if ($line["code_site"] != "J5--0220") {
                $point_geom = "";
                $lon = "";
                $lat = "";
            }
            $dataSample = array(
                "sample_id" => 0,
                "origin_id" => $origin_id,
                "taxon_id" => 1,
                "sample_date" => $line["date_capture"],
                "coordinate_precision" => $line["riviere"],
                "lon" => $lon,
                "lat" => $lat,
                "point_geom" => $point_geom
            );
            $sample_id = $sample->ecrire($dataSample);
            $samplesRecorded++;
        }
        $individual_id = $individual->getIdFromIdentifier($origin_id, $line["individu_identifier"]);
        if ($individual_id == 0) {
            $dataIndividual = array(
                "individual_id" => 0,
                "sample_id" => $sample_id,
                "tag" => $line["tag"],
                "identifier" => $line["individu_identifier"],
                "age" => $line["Age"],
                "total_length" => $line["Longueur_totale"],
                "fork_length" => $line["Longueur_fourche"],
                "weight" => $line["Poids"]
            );
            $individual->ecrire($dataIndividual);
            $individualRecorded++;
        }
    }
}
$message->set("$samplesRecorded recorded samples on a total of $totalLines treated");
$message->set("$individualRecorded individuals recorded");
$csv->fileClose();
