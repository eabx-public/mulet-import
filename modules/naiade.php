<?php
include_once "class/sample.class.php";
include_once "class/origin.class.php";
$sample = new Sample($pdo, $ObjetBDDParam);
$sample->auto_date = 0;
$origin = new Origin($pdo, $ObjetBDDParam);
$origin_id = $origin->getIdFromName($module["origin"]);
$csv = new Csv();
$csv->initFile($module["filename"], $module["separator"]);
$eof = false;
$totalLines = 0;
$recordedLines = 0;
while (!$eof) {
    $line = $csv->getLineAsArray();
    if (!$line) {
        $eof = true;
    } else {
        $totalLines++;
        $data = array(
            "sample_id" => 0,
            "origin_id" => $origin_id,
            "taxon_id" => 1,
            "sample_date" => $line["DateDebutOperationPrelBio"],
            "coordinate_precision" => $line["LbStationMesureEauxSurface"],
            "origin_precision" => $line["RefOperationPrelBio"],
            "total_number" => 1,
            "lon" => $line["lon"],
            "lat" => $line["lat"],
            "point_geom" => "POINT(" . $line["lon"] . " " . $line["lat"] . ")"
        );
        if ($line["CdTypTaxRep"] == 1) {
            $data["total_number"] = $line["RsTaxRep"];
        } else if ($line["CdTypTaxRep"] == 9) {
            $data["total_weight"] = $line["RsTaxRep"];
        }
        $sample->ecrire($data);
        $recordedLines++;
    }
}
$message->set("$recordedLines recorded on a total of $totalLines treated");
$csv->fileClose();
