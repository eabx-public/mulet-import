<?php
include_once "class/origin.class.php";
include_once "class/taxon.class.php";
include_once "class/annualFishing.class.php";
$taxon = new Taxon($pdo, $ObjetBDDParam);
$annualFishing = new AnnualFishing($pdo, $ObjetBDDParam);
$origin = new Origin($pdo, $ObjetBDDParam);
$origin_id = $origin->getIdFromName($module["origin"]);
$csv = new Csv();
$csv->initFile($module["filename"], $module["separator"]);
$eof = false;
$totalLines = 0;
$recordedLines = 0;
while (!$eof) {
    $line = $csv->getLineAsArray();
    $totalLines++;
    if (!$line) {
        $eof = true;
    } else {
        /**
         * Rename the taxon
         */
        if ($line["Taxon"] == "Autres especes") {
            $line["Taxon"] = "Others mugilidae";
        } else if ($line["Taxon"] == "Non identifié") {
            $line["Taxon"] = "Mugilidae ssp.";
        }
        if (!empty($line["VALUE"])) {
            $weight = $line["VALUE"] * 1000;
        } else {
            $weight = 0;
        }
        $annualFishing->ecrire(
            array(
                "annual_fishing_id" => 0,
                "taxon_id" => $taxon->getIdFromName($line["Taxon"]),
                "origin_id" => $origin_id,
                "fishing_zone" => $line["Name_Fr"],
                "fao_area"=>$line["AREA.CODE"],
                "year" => $line["PERIOD"],
                "live_weight" => $weight
            )
        );
        $recordedLines++;
    }
}
$message->set("$recordedLines recorded on a total of $totalLines treated");
$csv->fileClose();
