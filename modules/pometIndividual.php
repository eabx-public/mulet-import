<?php
include_once "class/sample.class.php";
include_once "class/individual.class.php";
include_once "class/origin.class.php";
include_once "lib/db.class.php";
$db = new Db();
$pdoPomet = $db->connect(
    $module["dsn"],
    $module["login"],
    openssl_decrypt($module["password"], "aes-256-cbc", $secretKey),
    $module["searchPath"]
);
$origin = new Origin($pdo, $ObjetBDDParam);
$origin_id = $origin->getIdFromName($module["origin"]);
$sample = new Sample($pdo, $ObjetBDDParam);
$sampleExists = $sample->getListIdsFromOriginKeys($origin_id);
$individual = new Individual($pdo, $ObjetBDDParam);
$individualExists = $individual->getListIdsFromOriginKeys($origin_id);
$totalNumber = 0;
$treated = 0;
$commit = 0;
$maxCommit = 1000;
$sql = "select  ech_id as echantillon_id, ind_id as individu_id,
        longueur, poids
        from individu i 
        join echantillon e on (fk_ech_id = ech_id) 
        where espece_id = 104
        order by ind_id";
$dataPomet = $db->execute($sql);
foreach ($dataPomet as $row) {
    $totalNumber++;
    if (!key_exists($row["individu_id"], $individualExists)) {
        $individual->ecrire(
            array(
                "individual_id" => 0,
                "origin_key" => $row["individu_id"],
                "sample_id" => $sampleExists[$row["echantillon_id"]],
                "fork_length" => $row["longueur"],
                "weight"=>$row["poids"]
            )
        );
        $treated++;
        $commit++;
        if ($commit >= $maxCommit) {
            $pdo->commit();
            $message->set("$treated items recorded");
            $message->display();
            $commit = 0;
            $pdo->beginTransaction();
        }
    }
}
$message->set("$treated items recorded on a total of $totalNumber lines");
