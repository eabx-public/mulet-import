<?php
include_once "class/origin.class.php";
include_once "class/taxon.class.php";
include_once "class/weekFishing.class.php";
$taxon = new Taxon($pdo, $ObjetBDDParam);
$weekFishing = new WeekFishing($pdo, $ObjetBDDParam);
$origin = new Origin($pdo, $ObjetBDDParam);
$origin_id = $origin->getIdFromName($module["origin"]);
$csv = new Csv();
$csv->initFile($module["filename"], $module["separator"]);
$eof = false;
$totalLines = 0;
$recordedLines = 0;
while (!$eof) {
    $line = $csv->getLineAsArray();  
    if (!$line) {
        $eof = true;
    } else {
        $totalLines++;
        /**
         * Rename the taxon
         */
        if ($line["Taxon"] == "Autres especes") {
            $line["Taxon"] = "Others mugilidae";
        } else if ($line["Taxon"] == "Non identifié") {
            $line["Taxon"] = "Mugilidae ssp.";
        }
        $weekFishing->ecrire(
            array(
                "week_fishing_id" => 0,
                "taxon_id" => $taxon->getIdFromName($line["Taxon"]),
                "origin_id" => $origin_id,
                "harbour" => $line["Criee"],
                "fao_area"=>$line["AREA.CODE"],
                "year" => $line["Annee"],
                "week"=>$line["Semaine"],
                "live_weight" => $line["Quantites"],
                "amount"=>$line["Montant"]
            )
        );
        $recordedLines++;
    }
}
$message->set("$recordedLines recorded on a total of $totalLines treated");
$csv->fileClose();
