<?php
include_once "class/sample.class.php";
include_once "class/engine.class.php";
include_once "class/physicochemical.class.php";
include_once "lib/db.class.php";
$db = new Db();
$pdoTransect = $db->connect(
    $module["dsn"],
    $module["login"],
    openssl_decrypt($module["password"],"aes-256-cbc", $secretKey),
    $module["searchPath"]
);
$sample = new Sample($pdo, $ObjetBDDParam);
$sample->auto_date = 0;
$exists = $sample->getListIdsFromOriginKeys(1);
$engine = new Engine($pdo, $ObjetBDDParam);
$physicochemical = new Physicochemical($pdo, $ObjetBDDParam);
$totalNumber = 0;
$treated = 0;
$commit = 0;
$maxCommit = 1000;
/**
 * search the data into dbtransect
 */
$sql = "select trait_id, echantillon_id, trait_debut::date, protocole_name, maree_coef, 
position_engin_name,
extract(min from trait_fin - trait_debut) as duration,
conductivite, oxygene, temp_eau, turbidite, salinite,
volume_filtre,
site_transect_name, rive_libelle, wgs84_x, wgs84_y,
echantillon_id, nombre, masse, abondance, densite,
type_code
,materiel_description
from echantillon e 
join peche p using (peche_id)
join trait using (trait_id)
join site using (site_id)
join site_transect using (site_transect_id)
join protocole p2 using (protocole_id)
join position_engin using (position_engin_id)
join espece_type using (espece_type_id)
left outer join type using (type_id)
left outer join rive using (rive_id)
left outer join materiel using (materiel_id)
where code_csp = 'MUP'
order by trait_id, echantillon_id ";
$dataTransect = $db->execute($sql);
foreach ($dataTransect as $row) {
    $totalNumber ++;
    if (!key_exists($row["echantillon_id"], $exists)) {
        if (!empty ($row["wgs84_x"]) && !empty($row["wgs84_y"])) {
            $point = "point(" . $row["wgs84_x"] . " " . $row["wgs84_y"] . ")";
        } else {
            $point = "";
        }
        if ($row["type_code"] == "juv") {
            $stage_id = 2;
        } else if ($row["type_code"] == "adu") {
            $stage_id = 1;
        } else {
            $stage_id = "";
        }
        $data = array(
            "sample_id" => 0,
            "origin_id" => 1,
            "taxon_id" => 1,
            "engine_id" => $engine->getIdFromName($row["materiel_description"]),
            "stage_id" => $stage_id,
            "sample_date" => $row["trait_debut"],
            "duration" => $row["duration"],
            "total_number" => $row["nombre"],
            "total_weight" => $row["masse"],
            "lon" => $row["wgs84_x"],
            "lat" => $row["wgs84_y"],
            "point_geom" => $point,
            "origin_precision" => $row["protocol_name"]
                . " - " . $row["site_transect_name"]
                . " - " . $row["rive_libelle"]
                . " - " . $row["position_engin_name"],
            "origin_key" => $row["echantillon_id"]
        );
        $sample_id = $sample->ecrire($data);
        $physicochemical->ecrire(
            array(
                "physicochemical_id" => 0,
                "sample_id" => $sample_id,
                "conductivity" => $row["conductivite"],
                "oxygen_mgl" => $row["oxygene"],
                "temperature" => $row["temp_eau"],
                "turbidity" => $row["turbidite"],
                "salinity" => $row["salinite"]
            )
        );
        $treated ++;
        $commit ++;
        if ($commit >= $maxCommit) {
            $pdo->commit();
            $message->set("$treated items recorded");
            $message->display();
            $commit = 0;
            $pdo->beginTransaction();
        }
    }
}
$message->set("$treated items recorded on a total of $totalNumber lines");
