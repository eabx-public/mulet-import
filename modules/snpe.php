<?php
include_once "class/sample.class.php";
include_once "class/origin.class.php";
include_once "class/taxon.class.php";
include_once "class/engine.class.php";
$sample = new Sample($pdo, $ObjetBDDParam);
//$sample->auto_date = 0;
$taxon = new Taxon($pdo, $ObjetBDDParam);
$origin = new Origin($pdo, $ObjetBDDParam);
$origin_id = $origin->getIdFromName($module["origin"]);
$engine = new Engine($pdo, $ObjetBDDParam);
$csv = new Csv();
for ($year = 2009; $year <= 2020; $year++) {
    $filename = "SNPE_" . $year . "_ponapomi.csv";
    $csv->initFile($module["folder"] . $filename, $module["separator"]);
    $eof = false;
    $totalLines = 0;
    $recordedLines = 0;
    while (!$eof) {
        $line = $csv->getLineAsArray();

        if (!$line) {
            $eof = true;
        } else {
            $totalLines++;
            /**
             * Search for taxon
             */
            $taxon_prise = trim($line["ID_TAXON_PRISE"]);
            $taxon_id = 0;
            if ($taxon_prise == 2178 || $line["LIBELLE_TAXON_PRISE"] == "Mugilidés") {
                $taxon_id = 4;
            } else if ($taxon_prise == 2180) {
                $taxon_id = 5;
            } else if ($taxon_prise == 2185 || $line["LIBELLE_TAXON_PRISE"] == "Mulet cabot") {
                $taxon_id = 8;
            } else if ($taxon_prise == 2183 || $line["LIBELLE_TAXON_PRISE"] == "Mulet porc") {
                $taxon_id = 1;
            }
            if ($taxon_id > 0) {
                /**
                 * Treatment of the date
                 */
                (!empty($line["DATE_EFFORT"])) ? $date = $line["DATE_EFFORT"] : $date = $line["DATE_UTIL"];
                (!empty($line["CT_ID_PRISE"]) && !empty($line["POIDS_DIV"])) ? $weight = $line["CT_ID_PRISE"] * $line["POIDS_DIV"] * 1000 : $weight = "";
                (!empty($line["LIBELLE_ENGIN"])) ? $engine_id =  $engine->getIdFromName($line["LIBELLE_ENGIN"]) : $engine_id = "";
                $sample_id = $sample->ecrire(
                    array(
                        "sample_id" => 0,
                        "origin_id" => $origin_id,
                        "taxon_id" => $taxon_id,
                        "sample_date" => $date,
                        "total_weight" => $weight,
                        "origin_precision" => $line["LIBELLE_SECTEUR"],
                        "engine_id" => $engine_id
                    )
                );
                $recordedLines++;
            }
        }
    }
    $message->set("File $filename treated");
    $message->set("$recordedLines recorded on a total of $totalLines");
    $message->display();
    $csv->fileClose();
}
