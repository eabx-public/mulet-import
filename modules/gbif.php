<?php
include_once "class/sample.class.php";
include_once "class/origin.class.php";
include_once "class/taxon.class.php";
$sample = new Sample($pdo, $ObjetBDDParam);
$sample->auto_date = 0;
$taxon = new Taxon($pdo, $ObjetBDDParam);
$origin = new Origin($pdo, $ObjetBDDParam);
$origin_id = $origin->getIdFromName($module["origin"]);
$csv = new Csv();
$csv->initFile($module["filename"], $module["separator"]);
$eof = false;
$totalLines = 0;
$recordedLines = 0;
while (!$eof) {
    $line = $csv->getLineAsArray();
    $totalLines++;
    if (!$line) {
        $eof = true;
    } else {
        /**
         * Treatment of the date
         */
        $date = "";
        if (!empty($line["year"])) {
            $date .= $line["year"] . "-";
            if (!empty($line["month"])) {
                $date .=  str_pad($line["month"], 2, '0', STR_PAD_LEFT);
            } else {
                $date .= "01";
            }
            $date .= "-";
            if (!empty($line["day"])) {
                $date .=  str_pad($line["day"], 2, '0', STR_PAD_LEFT);
            } else {
                $date .= "01";
            }
        }
        /**
         * Treatment of point
         */
        if ($line["decimalLongitude"]>99 || $line["decimalLatitude"]<99) {
            $line ["decimalLongitude"] = $line["decimalLongitude"] / 100;
        }
        $point = "point(" . $line["decimalLongitude"] . " " . $line["decimalLatitude"] . ")";
        $sample_id = $sample->ecrire(
            array(
                "sample_id" => 0,
                "origin_id" => $origin_id,
                "taxon_id" => 1,
                "sample_date" => $date,
                "total_number" => intval($line["individual_count"]),
                "lon" => $line["decimalLongitude"],
                "lat" => $line["decimalLatitude"],
                "point_geom" => $point,
                "origin_precision" => trim($line["institutionCode"] . " " . $line["references"]),
                "coordinate_uncertainty" => intval($line["coordinateUncertaintyInMeters"])
            )
        );
        $recordedLines++;
    }
}
$message->set("$recordedLines recorded on a total of $totalLines treated");
$csv->fileClose();
