<?php
include_once "lib/db.class.php";
include_once "class/historicalBasin.class.php";
include_once "class/origin.class.php";
$hb = new HistoricalBasin($pdo, $ObjetBDDParam);
$origin = new Origin($pdo, $ObjetBDDParam);
$origin_id = $origin->getIdFromName($module["origin"]);
$db = new Db();
$db->connect(
    $module["dsn"],
    $module["login"],
    openssl_decrypt($module["password"], "aes-256-cbc", $secretKey),
    $module["searchPath"]
);
$totalNumber = 0;
$treated = 0;
$sql = "select basin_name, year_from, year_to, presence_absence ,
        abundance_migratory as abundance_status , abundance_migratory_comment as abundance_comment
        ,st_x(seaoutlet_geom) as seaoutlet_lon, st_y(seaoutlet_geom) as seaoutlet_lat, st_astext(seaoutlet_geom) as seaoutlet_geom
        from eurodiad4.v_abundance va 
        left outer join eurodiad4.basin_outlet bo using (basin_id)
        where latin_name = 'Liza ramada'
        and presence_absence is not null
        order by basin_name, year_from";
$dataEurodiad = $db->execute($sql);
foreach ($dataEurodiad as $row) {
    $totalNumber++;
    $row["historical_basin_id"] = 0;
    $row["origin_id"] = $origin_id;
    $row["taxon_id"] = 1;
    $hb->ecrire($row);
}
$message->set("$totalNumber lines recorded");