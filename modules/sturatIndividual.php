<?php
include_once "class/sample.class.php";
include_once "class/individual.class.php";
include_once "class/origin.class.php";
include_once "lib/db.class.php";
$db = new Db();
$pdoSturat = $db->connect(
    $module["dsn"],
    $module["login"],
    openssl_decrypt($module["password"], "aes-256-cbc", $secretKey),
    $module["searchPath"]
);
$origin = new Origin($pdo, $ObjetBDDParam);
$origin_id = $origin->getIdFromName($module["origin"]);
$sample = new Sample($pdo, $ObjetBDDParam);
$sampleExists = $sample->getListIdsFromOriginKeys($origin_id);
$individual = new Individual($pdo, $ObjetBDDParam);
$individualExists = $individual->getListIdsFromOriginKeys($origin_id);
$totalNumber = 0;
$treated = 0;
$commit = 0;
$maxCommit = 1000;
$sql = "select  echantillon_id, individu_id , fork_length * 10 as fork_length_mm 
        from sturat.individu i 
        join sturat.echantillon e using (echantillon_id)
        where espece_id = 104
        order by echantillon_id, individu_id
        ";
$dataSturat = $db->execute($sql);
foreach ($dataSturat as $row) {
    $totalNumber++;
    if (!key_exists($row["individu_id"], $individualExists)) {
        $individual->ecrire(
            array(
                "individual_id" => 0,
                "origin_key" => $row["individu_id"],
                "sample_id" => $sampleExists[$row["echantillon_id"]],
                "fork_length" => $row["fork_length_mm"]
            )
        );
        $treated++;
        $commit++;
        if ($commit >= $maxCommit) {
            $pdo->commit();
            $message->set("$treated items recorded");
            $message->display();
            $commit = 0;
            $pdo->beginTransaction();
        }
    }
}
$message->set("$treated items recorded on a total of $totalNumber lines");
