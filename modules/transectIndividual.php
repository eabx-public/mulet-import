<?php
include_once "class/sample.class.php";
include_once "class/individual.class.php";
include_once "lib/db.class.php";
$db = new Db();
$pdoTransect = $db->connect(
    $module["dsn"],
    $module["login"],
    openssl_decrypt($module["password"], "aes-256-cbc", $secretKey),
    $module["searchPath"]
);
$sample = new Sample($pdo, $ObjetBDDParam);
$sampleExists = $sample->getListIdsFromOriginKeys(1);
$individual = new Individual($pdo, $ObjetBDDParam);
$individualExists = $individual->getListIdsFromOriginKeys(1);
$totalNumber = 0;
$treated = 0;
$commit = 0;
$maxCommit = 1000;
$sql = "select  echantillon_id, individu_id, longueur_fourche , longueur_fourche_by_classe , individu_masse ,
        type_code
        from individu i 
        join echantillon e using (echantillon_id)
        join espece_type using (espece_type_id)
        left outer join \"type\" using (type_id)
        where code_csp = 'MUP'
        order by echantillon_id, individu_id 
";
$dataTransect = $db->execute($sql);
foreach ($dataTransect as $row) {
    $totalNumber++;
    if (!key_exists($row["individu_id"], $individualExists)) {
        if (empty($row["longueur_fourche"])) {
            $lf = $row["longueur_fourche_by_classe"];
        } else {
            $lf = $row["longueur_fourche"];
        }
        if ($row["type_code"] == "juv") {
            $stage_id = 2;
        } else if ($row["type_code"] == "adu") {
            $stage_id = 1;
        } else {
            $stage_id = "";
        }
        $individual->ecrire(
            array(
                "individual_id" => 0,
                "origin_key" => $row["individu_id"],
                "sample_id" => $sampleExists[$row["echantillon_id"]],
                "stage_id" => $stage_id,
                "fork_length" => $lf,
                "weight" => $row["individu_masse"]
            )
        );
        $treated++;
        $commit++;
        if ($commit >= $maxCommit) {
            $pdo->commit();
            $message->set("$treated items recorded");
            $message->display();
            $commit = 0;
            $pdo->beginTransaction();
        }
    }
}
$message->set("$treated items recorded on a total of $totalNumber lines");
