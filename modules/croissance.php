<?php
include_once "class/lengthAge.class.php";
include_once "class/lengthAgeValue.class.php";
$la = new LengthAge($pdo, $ObjetBDDParam);
$lav = new LengthAgeValue($pdo, $ObjetBDDParam);
$csv = new Csv();
$csv->initFile($module["filename"], $module["separator"]);
$eof = false;
$totalLines = 0;
$recordedYears = 0;
while (!$eof) {
    $line = $csv->getLineAsArray();  
    if (!$line) {
        $eof = true;
    } else {
        $totalLines++;
        $dla = array (
            "length_age_id"=>0,
            "taxon_id"=>1,
            "author"=>$line["Authority"],
            "year"=>$line["Year"],
            "area"=>$line["Area"],
            "geographic_area"=>$line["Geographic area"],
            "environment"=>$line["Environ"],
            "method"=>$line["Method"],
            "lat"=>$line["Latitude"],
            "lon"=>$line["Longitude"],
            "geom"=>"point(" . $line["Longitude"] . " " . $line["Latitude"] . ")"
        );
        $laId = $la->ecrire($dla);
        for($age = 1; $age <= 9 ; $age ++) {
            if (!empty($line["L$age"])) {
                $dlav = array(
                    "length_age_value_id"=>0,
                    "length_age_id"=>$laId,
                    "age"=>$age,
                    "length"=>$line["L$age"]
                );
                $lav->ecrire($dlav);
                $recordedYears++;
            }
        }
    }
}
$message->set("$recordedYears annual sizes recorded from $totalLines lines read");