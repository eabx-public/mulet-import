<?php
include_once "class/sample.class.php";
include_once "class/engine.class.php";
include_once "class/physicochemical.class.php";
include_once "class/origin.class.php";
include_once "class/trace.class.php";
include_once "lib/db.class.php";
$db = new Db();
$pdoSturat = $db->connect(
    $module["dsn"],
    $module["login"],
    openssl_decrypt($module["password"], "aes-256-cbc", $secretKey),
    $module["searchPath"]
);
$origin = new Origin($pdo, $ObjetBDDParam);
$origin_id = $origin->getIdFromName($module["origin"]);
$sample = new Sample($pdo, $ObjetBDDParam);
$sample->auto_date = 0;
$exists = $sample->getListIdsFromOriginKeys($origin_id);
$engine = new Engine($pdo, $ObjetBDDParam);
$physicochemical = new Physicochemical($pdo, $ObjetBDDParam);
$trace = new Trace($pdo, $ObjetBDDParam);

$totalNumber = 0;
$treated = 0;
$commit = 0;
$maxCommit = 1000;
/**
 * search the data into db sturat
 */
$sql = "select trait_id, echantillon_id,  trait_start::date, extract('min' from duration) as duration,
        station_code,
        st_x(st_centroid(geom)) as st_x,
        st_y(st_centroid(geom)) as st_y,
        weight, total_number , total_measured_number , total_sample , sample_comment , weight_comment
        ,temperature, conductivity,salinity, oxygen_ppt, oxygen_mgl, turbidity, ph
        ,engin_type_libelle
        ,st_astext(trace_geom) as trace_geom
        ,st_x(st_startpoint(trace_geom)) as start_x
        ,st_y(st_startpoint(trace_geom)) as start_y
        ,st_x(st_endpoint(trace_geom)) as end_x
        ,st_y(st_endpoint(trace_geom)) as end_y
        from sturat.echantillon e
        join sturat.trait t using (trait_id)
        join sturat.station using (station_id)
        left outer join sturat.trait_geom tg using (trait_id)
        left outer join sturat.physicochimie p using (trait_id)
        left outer join sturat.trait_trace using (trait_id)
        left outer join sturat.engin_type et using (engin_type_id)
        where espece_id = 104
        order by trait_id, echantillon_id  ";
$dataSturat = $db->execute($sql);
foreach ($dataSturat as $row) {
    $totalNumber++;
    if (!key_exists($row["echantillon_id"], $exists)) {
        if (!empty($row["st_x"]) && !empty($row["st_y"])) {
            $point = "point(" . $row["st_x"] . " " . $row["st_y"] . ")";
        } else {
            $point = "";
        }

        $data = array(
            "sample_id" => 0,
            "origin_id" => $origin_id,
            "taxon_id" => 1,
            "engine_id" => $engine->getIdFromName($row["engin_type_libelle"]),
            "sample_date" => $row["trait_start"],
            "duration" => $row["duration"],
            "total_number" => $row["total_number"],
            "total_weight" => $row["weight"],
            "lon" => $row["st_x"],
            "lat" => $row["st_y"],
            "point_geom" => $point,
            "origin_precision" => $row["station_code"],
            "origin_key" => $row["echantillon_id"]
        );
        $sample_id = $sample->ecrire($data);
        $physicochemical->ecrire(
            array(
                "physicochemical_id" => 0,
                "sample_id" => $sample_id,
                "conductivity" => $row["conductivity"],
                "oxygen_mgl" => $row["oxygen_mgl"],
                "oxigen_ppt" => $row["oxygen_ppt"],
                "temperature" => $row["temperature"],
                "turbidity" => $row["turbidity"],
                "salinity" => $row["salinity"],
                "ph" => $row["ph"]
            )
        );
        if (!empty($row["trace_geom"])) {
            $trace->ecrire(
                array(
                    "trace_id" => 0,
                    "sample_id" => $sample_id,
                    "start_lon" => $row["start_x"],
                    "start_lat" => $row["start_y"],
                    "end_lon" => $row["end_x"],
                    "end_lat" => $row["end_y"],
                    "trace_geom" => $row["trace_geom"]
                )
            );
        }
        $treated++;
        $commit++;
        if ($commit >= $maxCommit) {
            $pdo->commit();
            $message->set("$treated items recorded");
            $message->display();
            $commit = 0;
            $pdo->beginTransaction();
        }
    }
}
$message->set("$treated items recorded on a total of $totalNumber lines");
