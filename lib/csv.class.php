<?php

/**
 * Classe de gestion des imports csv
 *
 * @author quinton
 *
 */
class Csv
{
    private $separator = ";";
    private $handle;
    public $header = array();
    public $nbColumns = 0;

    /**
     * Init file function
     * Get the first line for header
     *
     * @param string  $filename
     * @param string  $separator
     * @param boolean $utf8_encode
     *
     * @throws importException
     */
    function initFile($filename, $separator = ";",  $headerLine = 1)
    {
        switch ($separator) {
            case "tab":
            case "t":
                $separator = "\t";
                break;
            case "comma":
                $separator = ",";
                break;
            case "semicolon":
                $separator = ";";
                break;
        }
        $this->separator = $separator;
        /*
         * File open
         */
        if (!file_exists($filename)) {
            throw new importException($filename . " not found");
        }
        if (!$this->handle = fopen($filename, 'r')) {
            throw new importException($filename . " not readable");
        }
        /**
         * Get the header
         */
        /**
         * Go to the line of the header
         */
        for ($i = 1; $i < $headerLine; $i++) {
            $this->readLine();
        }
        $this->header = $this->readLine();
        $this->nbColumns = count($this->header);
    }

    /**
     * Read a line
     *
     * @return array|bool
     */
    function readLine()
    {
        if ($this->handle) {
            return fgetcsv($this->handle, 0, $this->separator);
        } else {
            return false;
        }
    }

    /**
     * Read the csv file, and return an associative array
     *
     * @return mixed[][]
     */
    function getContentAsArray()
    {
        $data = array();
        while (($line = $this->readLine()) !== false) {
            $dl = array();
            for ($i = 0; $i < $this->nbColumns; $i++) {
                $dl[$this->header[$i]] = $line[$i];
            }
            $data[] = $dl;
        }
        return $data;
    }

    function getLineAsArray() :array|bool {
        $data = array();
        $line = $this->readLine();
        if ($line) {
            $data["KEY"]="test";
            for ($i = 0; $i < $this->nbColumns; $i++) {
                $data[$this->header[$i]] = $line[$i];
            }
            return $data;
        } else {
            return false;
        }
    }

    /**
     * Close the file
     */
    function fileClose()
    {
        if ($this->handle) {
            fclose($this->handle);
        }
    }
}
